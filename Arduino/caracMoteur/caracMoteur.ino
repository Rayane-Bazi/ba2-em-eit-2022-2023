/*
 * Code permettant de mesurer la vitesse maximum d'un moteur.  Il est base sur testmoteur et testEncodeur.
 * Le comptage des impulsions de l'encodeur est fait par la meme routine d'interruption que testEncodeur.
 * 
 * Le programme demande (via Serial) a l'utilisateur le nombres de mesures a faire.
 * Ensuite, il accelere le moteur jusqu'à sa vitesse max. L'acceleration est definie par PWM_STEP
 * Il envoie la mesure de l'encodeur toutes les 10ms (PERIOD) sur Serial.
 * Apres avoir envoye le nombre de mesures demande, il arrete le moteur et attend un nouvel ordre.
 */

// DEFINITION DES PATTES UTILISEES
//////////////////////////////////////////////////////
// Pattes de commande du moteur
#define DIR  2
#define PWM  3
// Pattes de l'encodeur
#define CHA  4
#define CHB  5

// DEFINTION DES CONSTANTES
//////////////////////////////////////////////////////
#define PERIOD    10  // periode d'acquisition des mesures, en ms
#define PWM_STEP  5   // increment de la PWM à chaque periode => acceleration en (255/5)*10ms = 510ms


// DEFINITION DES VARIABLES GLOBALES
//////////////////////////////////////////////////////
// variable de l'ISR
volatile long isrTickCount;
// Variables de la boucle principale
long tickCount;     // mesure de l'encodeur
long t0;            // temps de la dernière acquisition
int pwm;            // valeur de la pwm appliquee au moteur
int sampleNb;       // nombre de mesures demande
int sampleCount;    // nombre de mesures effectuees


void setup() {
  Serial.begin(9600);
  pinMode(DIR, OUTPUT);         // configure la patte DIR en sortie digitale
  pinMode(CHA, INPUT_PULLUP);   // configure les pattes de l'encodeur en entrée, a HIGH par defaut
  pinMode(CHB, INPUT_PULLUP);
  // Initialisation de l'ISR
  isrTickCount = 0;
  attachInterrupt(digitalPinToInterrupt(CHA), encoderIsr, CHANGE);
}


void loop() {
  Serial.println("Combien de points de mesures voulez-vous ?");
  while ( not( Serial.available() ) );  // on attend de recevoir quelque chose sur Serial
  sampleNb = Serial.parseInt();   // on suppose que c'est un 'int' 
  if (sampleNb > 0) {             // si ce n'est pas le cas, parseInt retourne 0
    sampleCount = 0;
    pwm = 0;
    t0 = millis();
    
    while (sampleCount < sampleNb) {  // on doit faire sampleNb mesures
      if (millis() > t0 + PERIOD) {   // on attend que PERIOD ms soit passée depuis la dernière mesures
        noInterrupts();             // section critique : on desactive l'ISR
        tickCount = isrTickCount;   // pour pouvoir lire sa variable de manière fiable
        interrupts();               // fin de la section critique
        sampleCount++;
        t0 = t0 + PERIOD;
        if (pwm < 256) {          // phase d'acceleration
          pwm = pwm + PWM_STEP;
          motSetVoltage(pwm);
        }
        Serial.println(tickCount);  // envoi de la mesure
      }
    }
    motSetVoltage(0);   // apres avoir fait toutes les mesures, on arrete le moteur
    Serial.println(""); // ajout d'une ligne vide a la fin, pour une mise en page plus jolie
  }
}


/* ISR de l'encodeur
 *  executee a chaque changement d'etat de la patte CHA
 *  
 *  ISR => pas de paramettre pas pas de valeur renvoyee.
 *  
 *  modifie la vaiable isrTickCount, qui contient la position angulaire du moteur, en 'ticks'
 */
void encoderIsr(void) {
  if (digitalRead(CHA) == digitalRead(CHB)) {   // a chaque changement de CHA
    isrTickCount++;                             // incremente ou decremente isrTickCount
  } else {                                      // en fonction de la valeur de CHB
    isrTickCount--;
  }
}


/* Fonction de commande du moteur
 * 
 * parametre :
 *   - pwm : valeur de la PWM a appliquer au moteur. -255 <= pwm <= 255
 *           son signe donne le sens de rotation du moteur
 *           
 * valeur renvoyee : aucune
 */
 void motSetVoltage(int pwm) {
  // determination du sens de rotation
  if (pwm < 0) {
    digitalWrite(DIR, HIGH);
    pwm = -pwm;
  } else {
    digitalWrite(DIR, LOW);
  }
  // Saturation a la valeur max de la PWM
  if (pwm > 255) {
    pwm = 255;
  }
  // mise a jour de la PWM appliquee au moteur
  analogWrite(PWM, pwm);
}
